compiling
compileRBAssignmentNode: aNode
|variablestr receiverstr selectorstr |

variablestr := aNode variable name asString.
receiverstr := aNode value receiver name asString.
selectorstr := aNode value selector asString.
(receiverstr  = 'charArray') ifTrue: [ selectorstr := 'char[',((aNode value arguments asArray) at:1) receiver value asString,' ]' ].
selectorstr = 'cpointer' ifTrue: [ selectorstr :='*' ] ifFalse: [selectorstr := selectorstr ,' '].
^ variablestr,' ',selectorstr, receiverstr,';'  

