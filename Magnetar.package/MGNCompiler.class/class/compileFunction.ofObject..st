compiling
compileFunction: amethod ofObject: aobject
|body mast statements  cstr |
cstr := ''.
mast := (aobject compiledMethodAt: amethod) ast.
body := mast body.
statements := body statements.
statements do:[:st | (st class == RBAssignmentNode ) ifTrue:[cstr := cstr, (self compileRBAssignmentNode: st),Character cr asString ]].

^cstr 